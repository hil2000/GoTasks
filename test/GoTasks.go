package main

import (
    "git.oschina.net/janpoem/GoTasks"
    "git.oschina.net/janpoem/go-logger"
)

func main() {
    defer GoTasks.TaskServiceStart()
    GoTasks.On("get:change", func(rs *GoTasks.TaskResult) {
        logger.Log(rs.Id, rs.Resp)
    })
}
